import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path
from datetime import datetime, timedelta

yesterday = datetime.now() - timedelta(days=1)
last_date = yesterday.strftime('%d-%B-%y')

email = 'support@signcatch.com'
password = '12qwaszx'
send_to_emails = [ 'manish.aswani@grofers.com', 'himanshu@signcatch.com', 'saurabh@signcatch.com','sahil@signcatch.com', 'vaishali@signcatch.com', 'aastha@signcatch.com', 'mahesh@signcatch.com', 'udit.sidana@signcatch.com', 'j.sanjay@signcatch.com', 'krishan.kohli@grofers.com', 'deepshri.somani@grofers.com'] # List of email addresses

subject = 'Reports for ' + last_date
message = 'Please find the daily reports for ' + last_date + ' as attached below.'
file_location = [ '/home/ubuntu/csv/customerpurchases/customerpurchases'+last_date+'.csv', '/home/ubuntu/csv/sales/sales'+last_date+'.csv', '/home/ubuntu/csv/vendorpurchase/vendorpurchase'+last_date+'.csv' , '/home/ubuntu/csv/promotionreport/promotionreport'+last_date+'.csv']

# Create the attachment file (only do it once)
#for f in file_location:
#    filename = os.path.basename(f)
#    attachment = open(f, "rb")
#    part = MIMEBase('application', 'octet-stream')
#    part.set_payload(attachment.read())
#    encoders.encode_base64(part)
#    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
#    msg.attach(part)
# Connect and login to the email server
server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(email, password)

# Loop over each email to send to
for send_to_email in send_to_emails:
    # Setup MIMEMultipart for each email address (if we don't do this, the emails will concat on each email sent)
    msg = MIMEMultipart()
    msg['From'] = "SignCatchSupport <support@signcatch.com>"
    msg['To'] = send_to_email
    msg['Subject'] = subject

    # Attach the message to the MIMEMultipart object
    for f in file_location:
        filename = os.path.basename(f)
        attachment = open(f, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

        #msg.attach(MIMEText(message, 'plain'))
    # Attach the attachment file
        msg.attach(part)
    msg.attach(MIMEText(message, 'plain'))
    # Send the email to this specific email address
    server.sendmail(email, send_to_email, msg.as_string())

# Quit the email server when everything is done
server.quit()

