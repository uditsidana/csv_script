import MySQLdb
import csv
import sys
import os
from datetime import datetime, timedelta

yesterday = datetime.now() - timedelta(days=1)
last_date = yesterday.strftime('%d-%B-%y')

connection = MySQLdb.connect (host = "readreplicaprod.cgqzuunihdzt.ap-south-1.rds.amazonaws.com",
                              user = "Prod_SignCatch",
                              passwd = "PIg%ECa7!2F%",
                              db = "Prod_SignCatch")

dbQuery = "SELECT `mv`.`vendor_name`, `m`.`company_name` AS `merchant_name`, po.store_id, `mp`.`location` AS `store_name`, po.po_no, po.grn_no, po.invoice_id, po.invoice_date, po.po_total, po.grand_total, po.expected_receiving_date, po.grn_date, op.product_name, op.batch_id as batch_no, op.sku, op.hsn, op.barcode, op.demanded_qty, op.demanded_free_qty, op.received_qty, op.free_qty as received_free_qty, op.uom, op.total_price, op.disc_perc, op.disc_value, op.tax_group, op.total_tax, op.cess_amt, op.cess_per, op.add_cess_rate, op.add_cess_value, op.basic_cp, op.cp, op.sp, op.mrp, op.margin_cp, op.margin_mrp, op.margin_perc, po.received_at, po.purchase_order_date FROM `merchant_vendors` `mv` INNER JOIN `purchasing_order` `po` ON po.vendor_id = mv.id LEFT JOIN `purchasing_order_product` op ON po.id = op.purchasing_order_id AND op.purchasing_order_id INNER JOIN `merchant_pickup` `mp` ON po.store_id = mp.id INNER JOIN `merchant` `m` ON m.id = mv.merchant_id WHERE m.enterprise_ref_id = 1 AND m.status = 1 AND po.status = 3 AND m.merchant_account_type = 1 AND DATE(po.created_at) >=  DATE(NOW() - INTERVAL 1 DAY) AND DATE(po.created_at) <=  DATE(NOW() - INTERVAL 1 DAY);"
cur=connection.cursor()
cur.execute(dbQuery)
rows=cur.fetchall()
column_names = [i[0] for i in cur.description]
fp = open('/home/ubuntu/csv/vendorpurchase/vendorpurchase.csv', 'w')
myFile = csv.writer(fp, lineterminator = '\n')
myFile.writerow(column_names)
myFile.writerows(rows)
fp.close()
newname= '/home/ubuntu/csv/vendorpurchase/vendorpurchase'+last_date+'.csv'
os.rename('/home/ubuntu/csv/vendorpurchase/vendorpurchase.csv', newname)

