import MySQLdb
import csv
import sys
import os
from datetime import datetime, timedelta

yesterday = datetime.now() - timedelta(days=1)
last_date = yesterday.strftime('%d-%B-%y')

connection = MySQLdb.connect (host = "readreplicaprod.cgqzuunihdzt.ap-south-1.rds.amazonaws.com",
                              user = "Prod_SignCatch",
                              passwd = "PIg%ECa7!2F%",
                              db = "Prod_SignCatch")

dbQuery = "SELECT op.id, pbl.id as batch_id, op.product_name, op.product_id, op.sku,op.barcode, op.quantity, op.pre_tax_amount, op.tax_amount, op.discount, (op.pre_tax_amount + op.tax_amount) AS total, (case when(op.product_id = 0) THEN (1) ELSE 0 END) as is_miscellaneous,op.brand_name,op.category_name,op.department_name,op.quantity_unit,op.hsn,op.batch_no,op.list_price,op.price,u.id as user_id,u.first_name,u.last_name,u.isd_code,u.phone_number,u.address,u.city,u.state,u.country, DATE(op.created_at),o.amount as order_amount ,o.order_id as order_no,o.refund_order,m.company_name as merchant_name,mp.location as store_name,o.created_at,o.updated_at,(o.discount_amount) as order_dicount,o.tax_amount,o.store_id FROM order_product op LEFT JOIN `order` o ON o.id = op.order_id LEFT JOIN merchant m ON o.merchant_id = m.id LEFT JOIN merchant_pickup mp ON o.store_id = mp.id  LEFT JOIN user u ON o.user_id=u.id  LEFT JOIN product_batch_location pbl ON pbl.location_id=o.store_id AND pbl.product_id=op.product_id AND pbl.batch_no=op.batch_no WHERE op.deleted = 0 AND o.transaction_status=3 AND o.user_id>0 AND m.enterprise_ref_id=1 AND m.merchant_account_type=1 AND DATE(o.created_at) >=  DATE(NOW() - INTERVAL 1 DAY) AND DATE(o.created_at) <=  DATE(NOW() - INTERVAL 1 DAY) GROUP BY op.id  ORDER BY op.id;"
cur=connection.cursor()
cur.execute(dbQuery)
rows=cur.fetchall()
column_names = [i[0] for i in cur.description]
fp = open('/home/ubuntu/csv/customerpurchases/customerpurchases.csv', 'w')
myFile = csv.writer(fp, lineterminator = '\n')
myFile.writerow(column_names)
myFile.writerows(rows)
fp.close()
newname= '/home/ubuntu/csv/customerpurchases/customerpurchases'+last_date+'.csv'
os.rename('/home/ubuntu/csv/customerpurchases/customerpurchases.csv', newname)
                                   
