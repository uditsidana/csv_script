import MySQLdb
import csv
import sys
import os
from datetime import datetime, timedelta

yesterday = datetime.now() - timedelta(days=1)
last_date = yesterday.strftime('%d-%B-%y')

connection = MySQLdb.connect (host = "prod-db-01-signcatch.cgqzuunihdzt.ap-south-1.rds.amazonaws.com",
                              user = "Prod_SignCatch",
                              passwd = "PIg%ECa7!2F%",
                              db = "Prod_SignCatch")


initiate = "create or replace view `product_tax_info` AS SELECT pt.product_id, pt.tax_id, mt.name, mt.tax_type FROM `product_tax` pt LEFT JOIN `master_tax` mt ON pt.tax_id = mt.id;"
cur=connection.cursor()
cur.execute(initiate)

dbQuery = "SELECT mp.location as 'Store Name', pb.location_id as store_id, p.favorite_product as is_master, p.name, p.sku as product_id, u.name as uom, mtg.name as tax_group, p.hsn,pt1.tax_id as cess_id, pt2.tax_id as addcess_id, pb.barcode, pb.batch_no, pb.quantity as inventory, pb.backoffice_quantity, pb.rackspace_quantity,pb.cost_price, pb.sale_price_without_discount as mrp, pb.sale_price_with_discount as selling_price, pb.expiry_date, d.department_name as department, c.category as category, p.brand_name as brand, p.minimum_order_quantity as min_order_limit, p.product_thumbnail as image, p.description  FROM `product` p   LEFT JOIN `product_batch_location` pb ON  pb.product_id = p.id AND pb.deleted=0  LEFT JOIN `merchant_pickup` mp ON  pb.location_id = mp.id LEFT JOIN `product_departments` d ON p.department_id = d.id LEFT JOIN `product_category` c ON p.category_id = c.id  LEFT JOIN `master_tax_group` mtg ON p.master_tax_group_id = mtg.id  LEFT JOIN `product_tax_info` pt1 ON p.id = pt1.product_id AND pt1.tax_type = 4 LEFT JOIN `product_tax_info` pt2 ON p.id = pt2.product_id AND pt2.tax_type = 5 LEFT JOIN `product_uom` u ON p.uom = u.id LEFT JOIN `merchant` m ON m.id = p.merchant_id WHERE p.deleted =0 AND pb.deleted = 0 AND `p`.merchant_id IN (select id from `merchant` where m.status = 1 AND m.merchant_account_type = 1 AND m.enterprise_ref_id = 1);"

cur=connection.cursor()
cur.execute(dbQuery)
rows=cur.fetchall()
column_names = [i[0] for i in cur.description]
fp = open('/home/ubuntu/csv/merchantcataloguedata/merchantcataloguedata.csv', 'w')
myFile = csv.writer(fp, lineterminator = '\n')
myFile.writerow(column_names)
myFile.writerows(rows)
fp.close()
newname= '/home/ubuntu/csv/merchantcataloguedata/merchantcataloguedata'+last_date+'.csv'
os.rename('/home/ubuntu/csv/merchantcataloguedata/merchantcataloguedata.csv', newname)

