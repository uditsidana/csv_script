import MySQLdb
import csv
import sys
import os
from datetime import datetime, timedelta

yesterday = datetime.now() - timedelta(days=1)
last_date = yesterday.strftime('%d-%B-%y')

connection = MySQLdb.connect (host = "readreplicaprod.cgqzuunihdzt.ap-south-1.rds.amazonaws.com",
                              user = "Prod_SignCatch",
                              passwd = "PIg%ECa7!2F%",
                              db = "Prod_SignCatch")

dbQuery = "SELECT o.order_id,o.refund_order, op.sku, m.company_name, o.store_id, mp.location as store_name, op.product_name, op.batch_no , op.barcode,(case when(op.product_id = 0) THEN (1) ELSE 0 END) as is_miscellaneous, op.quantity,op.pre_tax_amount,op.tax_amount,((op.list_price*op.quantity)-(op.pre_tax_amount+op.tax_amount)) as mrp_discount, ((op.price*op.quantity)-(op.pre_tax_amount+op.tax_amount)) as sp_discount,(op.pre_tax_amount + op.tax_amount) AS total, o.created_at, op.brand_name, op.category_name,op.department_name,op.quantity_unit,op.hsn, o.updated_at, op.list_price as MRP,op.price as Sale_Price FROM `order` o LEFT JOIN order_product op  ON o.id = op.order_id LEFT JOIN merchant m ON o.merchant_id=m.id LEFT JOIN merchant_pickup mp ON o.store_id = mp.id WHERE op.deleted = 0 AND  o.transaction_status=3 AND m.enterprise_ref_id='1' AND m.merchant_account_type=1 AND m.status=1 AND DATE(o.`created_at`) >=  DATE(NOW() - INTERVAL 1 DAY) AND DATE(o.`created_at`) <=  DATE(NOW() - INTERVAL 1 DAY)  ORDER BY op.id;"

cur=connection.cursor()
cur.execute(dbQuery)
rows=cur.fetchall()
column_names = [i[0] for i in cur.description]
fp = open('/home/ubuntu/csv/sales/sales.csv', 'w')
myFile = csv.writer(fp, lineterminator = '\n')
myFile.writerow(column_names)   
myFile.writerows(rows)
fp.close()
newname= '/home/ubuntu/csv/sales/sales'+last_date+'.csv'
os.rename('/home/ubuntu/csv/sales/sales.csv', newname)
